/**
 * @file
 * JavaScript file for the Clientside custom alert
 */

(function ($) {
  'use strict';
  Drupal.behaviors.cca = {
    attach: function (context, settings) {
      $('.ccalert-enabled', context).focus(function () {
        if (!$(this).hasClass('ccalert-processed')) {
          $(this).parent().find('.ccalert-message-div').remove();
          var alertMessage = $(this).attr('ccalert_text');
          var alertType = $(this).attr('ccalert_type');
          $(this).addClass('ccalert-processed');
          if (alertType === 'inline_alert') {
            $(this).parent().append('<div class="ccalert-message-div">' + alertMessage + '</div>');
          }
          else {
            $(this).focus();
            alert(alertMessage);
          }
        }
      });
    }
  };
}(jQuery));
