<?php

/**
 * @file
 * Clientside custom alert module for giving custom alert message for actions.
 *
 * Clientside custom alert module is to give custom alert message for custom
 * created ones either via alert or inline message.
 */

/**
 * Implements hook_menu().
 */
function clientside_custom_alert_menu() {
  $items['admin/structure/clientside-custom-alert'] = array(
    'title' => 'Clientside custom alert',
    'description' => 'Configure clientside custom alert.',
    'page callback' => 'clientside_custom_alert_admin_display',
    'page arguments' => array(),
    'access arguments' => array(
      'administer clientside custom alert',
    ),
  );
  $items['admin/structure/clientside-custom-alert/add'] = array(
    'title' => 'Add new Clientside alert',
    'page callback' => 'clientside_custom_alert_add_page',
    'type' => MENU_LOCAL_ACTION,
    'access callback' => 'user_access',
    'access arguments' => array(
      'administer clientside custom alert',
    ),
  );
  $items['admin/structure/clientside-custom-alert/%/edit'] = array(
    'title' => 'Edit the Clientside alert',
    'page callback' => 'clientside_custom_alert_edit_page',
    'page arguments' => array(3),
    'type' => MENU_CALLBACK,
    'access callback' => 'user_access',
    'access arguments' => array(
      'administer clientside custom alert',
    ),
  );
  $items['admin/structure/clientside-custom-alert/%/delete'] = array(
    'title' => 'Delete the Clientside alert',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('clientside_custom_alert_delete_confirm', 3),
    'type' => MENU_CALLBACK,
    'access callback' => 'user_access',
    'access arguments' => array(
      'administer clientside custom alert',
    ),
  );
  return $items;
}

/**
 * In admin displaying the custom alerts.
 */
function clientside_custom_alert_admin_display() {
  $rows = array();

  $cca_results = db_select('clientside_custom_alert', 'cca')
    ->fields('cca')
    ->execute()->fetchAll();

  if (count($cca_results)) {
    foreach ($cca_results as $cca_result) {
      $edit_link = l(t('Edit'), 'admin/structure/clientside-custom-alert/' . $cca_result->ccaid . '/edit');
      $rows[] = array(
        $cca_result->ccaid,
        $cca_result->name,
        $cca_result->entity_type,
        $cca_result->bundle,
        $cca_result->field_name,
        $edit_link,
      );
    }
  }
  else {
    // Empty row.
    $rows[] = array(array('data' => 'No custom alert added yet', 'colspan' => 6));
  }

  $header = array('ID', t('Alert Name'), t('Entity Type'), t('Bundle'),
    t('Fieldname'), t('Operation'),
  );
  $output = theme('table', array('header' => $header, 'rows' => $rows));
  return $output;
}

/**
 * Page callback to add a new clientside custom alert.
 */
function clientside_custom_alert_add_page() {
  return drupal_get_form('clientside_custom_alert_add_form');
}

/**
 * Form builder for the "add new custom alert" page.
 */
function clientside_custom_alert_add_form($form, &$form_state) {
  $form['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Custom alert name'),
    '#description' => t('Give proper readable custom alert name. Eg. Article Body Field Alert'),
    '#required' => TRUE,
    '#size' => 32,
    '#default_value' => '',
    '#maxlength' => 255,
  );

  $fields = array();

  // Node fields.
  $content_types = node_type_get_types();

  $results = db_select('clientside_custom_alert', 'cca')
    ->fields('cca')
    ->execute()
    ->fetchAll();

  foreach ($content_types as $value) {
    $bundle = $value->name;
    $field_instances = field_info_instances('node', $bundle);
    $content_type_field = 'title-node-' . $value->type;
    $fields[$content_type_field] = $value->title_label . '[:node-' . $bundle . ']';
    foreach ($field_instances as $key1 => $value1) {
      $content_type_field = $key1 . '-node-' . $value1['bundle'];
      $skip_field = FALSE;
      foreach ($results as $result) {
        if ($result->entity_type == 'node') {
          $result_field = $result->field_name . '-' . $result->entity_type . '-' . $result->bundle;
          if ($result_field == $content_type_field) {
            $skip_field = TRUE;
            break;
          }
        }
      }
      if ($skip_field) {
        continue;
      }
      $fields[$content_type_field] = $value1['label'] . '[:node-' . $bundle . ']';
    }
  }

  // User fields.
  $field_instances = field_info_instances('user', 'user');

  foreach ($field_instances as $key1 => $value1) {
    $fields[$key1 . '-user-user'] = $value1['label'] . '[:user-user]';
  }

  $form['fields'] = array(
    '#title' => 'Custom alert field',
    '#description' => t('Choose the custom alert field.'),
    '#type' => 'select',
    '#options' => $fields,
    '#required' => TRUE,
    '#access' => TRUE,
  );

  $form['alert_text'] = array(
    '#title' => t('Alert text'),
    '#description' => t('A custom alert message for the field'),
    '#type' => 'textfield',
    '#default_value' => '',
    '#access' => TRUE,
    '#required' => TRUE,
  );

  $alert_types = array(
    'js_alert' => 'Javascript Alert',
    'inline_alert' => 'Inline Alert',
  );
  $form['alert_type'] = array(
    '#title' => 'Alert Type',
    '#description' => t('Choose the custom alert type.'),
    '#type' => 'select',
    '#options' => $alert_types,
    '#required' => TRUE,
    '#access' => TRUE,
  );
  $form['save'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
    '#submit' => array(
      'clientside_custom_alert_add_form_save_submit',
    ),
  );
  $form['cancel'] = array(
    '#type' => 'submit',
    '#value' => t('Cancel'),
    '#submit' => array(
      'clientside_custom_alert_add_form_cancel_submit',
    ),
    '#limit_validation_errors' => array(),
  );
  return $form;
}

/**
 * Page callback to add form of clientside custom alert.
 */
function clientside_custom_alert_add_form_save_submit($form, &$form_state) {
  $custom_alert_name = $form_state['values']['name'];
  $field = $form_state['values']['fields'];
  $fields = explode('-', $field);
  $field_name = $fields[0];
  $entity_type = $fields[1];
  $bundle = $fields[2];
  $alert_text = $form_state['values']['alert_text'];
  $alert_type = $form_state['values']['alert_type'];

  // Save the alert and open the edit form.
  $fields = array(
    'entity_type' => $entity_type,
    'bundle' => $bundle,
    'field_name' => $field_name,
    'name' => $custom_alert_name,
    'alert_message' => $alert_text,
    'alert_type' => $alert_type,
    'created_at' => time(),
  );
  db_insert('clientside_custom_alert')
    ->fields($fields)
    ->execute();

  drupal_set_message(t('Your changes are saved.'));
  drupal_goto('admin/structure/clientside-custom-alert');
}

/**
 * Page callback to cancel clientside custom alert.
 */
function clientside_custom_alert_add_form_cancel_submit($form, &$form_state) {
  drupal_goto('admin/structure/clientside-custom-alert');
}

/**
 * Page callback to add a new clientside custom alert.
 */
function clientside_custom_alert_edit_page($ccaid) {
  return drupal_get_form('clientside_custom_alert_edit_form', $ccaid);
}

/**
 * Form builder for the edit custom alert page.
 */
function clientside_custom_alert_edit_form($form, &$form_state, $ccaid) {
  $results = db_select('clientside_custom_alert', 'cca')
    ->fields('cca')
    ->condition('cca.ccaid', $ccaid, '=')
    ->execute()
    ->fetchAssoc();

  $form['ccaid'] = array(
    '#type' => 'hidden',
    '#value' => $ccaid,
  );
  $form['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Custom alert name'),
    '#description' => t('Give proper readable custom alert name. Eg. Article Body Field Alert'),
    '#required' => TRUE,
    '#size' => 32,
    '#default_value' => $results['name'],
    '#maxlength' => 255,
    '#disabled' => TRUE,
  );

  $fields_default[] = $results['field_name'];
  $fields_default[] = $results['entity_type'];
  $fields_default[] = $results['bundle'];
  $field = implode('-', $fields_default);
  $fields = array($field => $field);

  $form['fields'] = array(
    '#title' => 'Custom alert field',
    '#description' => t('Choose the custom alert field.'),
    '#type' => 'select',
    '#options' => $fields,
    '#required' => TRUE,
    '#access' => TRUE,
    '#default_value' => $field,
    '#disabled' => TRUE,
  );

  $form['alert_text'] = array(
    '#title' => t('Alert text'),
    '#description' => t('A custom alert message for the field'),
    '#type' => 'textfield',
    '#default_value' => $results['alert_message'],
    '#access' => TRUE,
    '#required' => TRUE,
  );

  $alert_types = array(
    'js_alert' => 'Javascript Alert',
    'inline_alert' => 'Inline Alert',
  );
  $form['alert_type'] = array(
    '#title' => 'Alert Type',
    '#description' => t('Choose the custom alert type.'),
    '#type' => 'select',
    '#options' => $alert_types,
    '#required' => TRUE,
    '#default_value' => $results['alert_type'],
  );

  $form['save'] = array(
    '#type' => 'submit',
    '#value' => t('Update'),
    '#submit' => array(
      'clientside_custom_alert_edit_form_save_submit',
    ),
  );

  $form['cancel'] = array(
    '#type' => 'submit',
    '#value' => t('Cancel'),
    '#submit' => array(
      'clientside_custom_alert_add_form_cancel_submit',
    ),
    '#limit_validation_errors' => array(),
  );

  $form['delete'] = array(
    '#type' => 'submit',
    '#value' => t('Delete'),
    '#submit' => array('clientside_custom_alert_edit_form_delete_submit'),
  );
  return $form;
}

/**
 * Alert Edit form save functionality.
 */
function clientside_custom_alert_edit_form_save_submit($form, &$form_state) {
  $ccaid = $form_state['values']['ccaid'];
  $alert_text = $form_state['values']['alert_text'];
  $alert_type = $form_state['values']['alert_type'];

  // Save the alert and open the edit form.
  $fields = array(
    'alert_message' => $alert_text,
    'alert_type' => $alert_type,
    'created_at' => time(),
  );
  db_update('clientside_custom_alert')
    ->fields($fields)
    ->condition('ccaid', $ccaid, '=')
    ->execute();

  drupal_set_message(t('Your changes are saved.'));
  drupal_goto('admin/structure/clientside-custom-alert');
}

/**
 * Alert Edit form delete functionality.
 */
function clientside_custom_alert_edit_form_delete_submit($form, &$form_state) {
  $ccaid = $form_state['values']['ccaid'];
  drupal_goto('admin/structure/clientside-custom-alert/' . $ccaid . '/delete');
}

/**
 * Delete confirm function.
 */
function clientside_custom_alert_delete_confirm($form, &$form_state, $ccaid) {
  $description = "This option cannot be undone. Are you sure?";
  $yes = "Confirm";
  $question = "Do you really want to delete ?";
  $path = 'admin/structure/clientside-custom-alert/' . $ccaid . '/edit';

  $results = db_select('clientside_custom_alert', 'cca')
    ->fields('cca')
    ->condition('cca.ccaid', $ccaid, '=')
    ->execute()
    ->fetchAssoc();
  $form['ccaid'] = array(
    '#type' => 'hidden',
    '#value' => $ccaid,
  );
  $form['name'] = array(
    '#type' => 'hidden',
    '#value' => $results['name'],
  );

  $confirm = confirm_form($form, $question, $path, $description, $yes);
  return $confirm;
}

/**
 * Delete submit function.
 */
function clientside_custom_alert_delete_confirm_submit($form, &$form_state) {
  $ccaid = $form_state['values']['ccaid'];
  $name = $form_state['values']['name'];

  db_delete('clientside_custom_alert')
    ->condition('ccaid', $ccaid, '=')
    ->execute();

  drupal_set_message(t('Custom alert') . ' <strong><em>' . $name . '</em></strong> ' . t('is deleted.'));
  drupal_goto('admin/structure/clientside-custom-alert');
}

/**
 * Implements hook_form_alter().
 */
function clientside_custom_alert_form_alter(&$form, &$form_state, $form_id) {

  if (strstr($form_id, '_node_form')) {
    $results = db_select('clientside_custom_alert', 'cca')
      ->fields('cca')
      ->condition('cca.entity_type', 'node', '=')
      ->condition('cca.bundle', $form['type']['#value'], '=')
      ->execute()
      ->fetchAll();
    foreach ($results as $result) {
      if ($result->field_name == 'title') {
        $classes = isset($form[$result->field_name]['#attributes']) ? $form[$result->field_name]['#attributes']['class'] : array();
        $classes[] = 'ccalert-enabled';
        $form[$result->field_name]['#attributes']['class'] = $classes;
        $form[$result->field_name]['#attributes']['ccalert_text'] = $result->alert_message;
        $form[$result->field_name]['#attributes']['ccalert_type'] = $result->alert_type;
      }
      else {
        foreach ($form[$result->field_name][LANGUAGE_NONE] as $delta => $value) {
          if (is_numeric($delta)) {
            if (isset($value['#type']) && $value['#type'] == 'text_format') {
              $classes = $value['#attributes']['class'];
              $classes[] = 'ccalert-enabled';
              $form[$result->field_name][LANGUAGE_NONE][$delta]['#attributes']['class'] = $classes;
              $form[$result->field_name][LANGUAGE_NONE][$delta]['#attributes']['ccalert_text'] = $result->alert_message;
              $form[$result->field_name][LANGUAGE_NONE][$delta]['#attributes']['ccalert_type'] = $result->alert_type;
            }
            elseif (isset($value['value'])) {
              $classes = $value['value']['#attributes']['class'];
              $classes[] = 'ccalert-enabled';
              $form[$result->field_name][LANGUAGE_NONE][$delta]['value']['#attributes']['class'] = $classes;
              $form[$result->field_name][LANGUAGE_NONE][$delta]['value']['#attributes']['ccalert_text'] = $result->alert_message;
              $form[$result->field_name][LANGUAGE_NONE][$delta]['value']['#attributes']['ccalert_type'] = $result->alert_type;
            }
          }
          elseif (isset($form[$result->field_name][LANGUAGE_NONE]['#type'])) {
            $classes = isset($form[$result->field_name][LANGUAGE_NONE]['#attributes']) ? $form[$result->field_name][LANGUAGE_NONE]['#attributes']['class'] : array();
            $classes[] = 'ccalert-enabled';
            $form[$result->field_name][LANGUAGE_NONE]['#attributes']['class'] = $classes;
            $form[$result->field_name][LANGUAGE_NONE]['#attributes']['ccalert_text'] = $result->alert_message;
            $form[$result->field_name][LANGUAGE_NONE]['#attributes']['ccalert_type'] = $result->alert_type;
          }
        }
      }
    }
  }
}
